package deloitte.academy.mvc.run;

/**
 * @author jbaldovinos@externosdeloittemx.com
 * @version 1.0
 * @since 11/03/2020
 */
import java.util.ArrayList;

import deloitte.academy.mvc.controller.ListaController;
import deloitte.academy.mvc.model.Atributos;

/**
 * Clase principal que permite ejecutar el programa
 * 
 */
public class Run {

	public static final ArrayList<Atributos> lista = new ArrayList<Atributos>();

	public static void main(String[] args) {

		Atributos atributo = new Atributos();
		Atributos atributo1 = new Atributos("Peso", "Dollar", "CAD", "Centavo", "1", "2", "3", "4", "5", "7", "8", "9",
				"10", "11", 25, 26, 27, 28, 29, 30);
		/**
		 * Se llama al metodo que agrega un objeto al arreglo
		 * 
		 * 
		 */
		ListaController lA = new ListaController();
		lA.agregar(atributo1);
		System.out.println("Buscar nombre");

		/**
		 * Se llama al metodo que busca por nombre
		 * 
		 */
		atributo = lA.buscarNombre(0);
		System.out.println("Atributo Neto: " + atributo.getAtributoNeto() + "\nAtributo1" + atributo.getAtributoNeto1()
				+ "\nAtributo2" + atributo.getAtributoNeto2() + "\nAtributo3" + atributo.getAtributoNeto3()
				+ "\nAtributo4" + atributo.getAtributoNeto4() + "\nAtributo5" + atributo.getAtributoNeto5()
				+ "\nAtributo6" + atributo.getAtributoNeto6() + "\nAtributo7" + atributo.getAtributoNeto7()
				+ "\nAtributo8" + atributo.getAtributoNeto8() + "\nAtributo9" + atributo.getAtributoNeto9()
				+ "\nAtributo10" + atributo.getAtributoNeto10() + "Atributo11" + atributo.getAtributoNeto11()
				+ "\nAtributo12" + atributo.getAtributoNeto12() + "\nAtributo13" + atributo.getAtributoNeto13()
				+ "\nAtributo14" + atributo.getAtributoNeto14() + "\nAtributo15" + atributo.getAtributoNeto15()
				+ "\nAtributo16" + atributo.getAtributoNeto16() + "\nAtributo17" + atributo.getAtributoNeto17()
				+ "\nAtributo18" + atributo.getAtributoNeto18() + "\nAtributo19" + atributo.getAtributoNeto19());

		/**
		 * Se llama al metodo que busca a todos los elementos del arreglo
		 */
		System.out.println("Buscar Todos");
		lA.buscarTodos();

	}
}