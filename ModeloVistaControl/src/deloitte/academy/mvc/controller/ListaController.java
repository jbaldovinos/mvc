package deloitte.academy.mvc.controller;

import java.util.function.Consumer;
import deloitte.academy.mvc.model.Atributos;
import deloitte.academy.mvc.run.Run;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ListaController {
	private static final Logger LOGGER = Logger.getLogger(Logger.class.getName());

	Atributos atributoPrueba = new Atributos();

	/**
	 * Metodo que agrega un objeto (atributo) a la lista
	 * 
	 * @param correr
	 */

	public void agregar(Atributos correr) {
		try {
			Run.lista.add(correr);
			LOGGER.info("Atribunto se agrego de manera correcta");
		} catch (Exception ex) {
			LOGGER.log(Level.SEVERE, "No se agrego de manera correcta", ex);
		}

	}

	/**
	 * 
	 * @param argumento1  variable tipo entero
	 * @param argumento2  variable tipo entero
	 * @param argumento3  variable tipo entero
	 * @param argumento4  variable tipo entero
	 * @param argumento5  variable tipo entero
	 * @param argumento6  variable tipo entero
	 * @param argumento7  variable tipo entero
	 * @param argumento8  variable tipo entero
	 * @param argumento9  variable tipo entero
	 * @param argumento10 variable tipo entero
	 * @param argumento11 variable tipo entero
	 * @param argumento12 variable tipo entero
	 * @param argumento13 variable tipo entero
	 * @param argumento14 variable tipo entero
	 * @param argumento15 variable tipo entero
	 * @param argumento16 variable tipo entero
	 * @param argumento17 variable tipo entero
	 * @param argumento18 variable tipo entero
	 * @param argumento19 variable tipo entero
	 * @param argumento20 variable tipo entero
	 * @throws ERROR
	 */
	public void agregarAtributo(int argumento1, int argumento2, int argumento3, int argumento4, int argumento5,
			int argumento6, int argumento7, int argumento8, int argumento9, int argumento10, int argumento11,
			int argumento12, int argumento13, int argumento14, int argumento15, int argumento16, int argumento17,
			int argumento18, int argumento19, int argumento20) {
		try {
			/**
			 * Se a�aden los atributos
			 * 
			 */
			atributoPrueba.setAtributoNeto(argumento1);
			atributoPrueba.setAtributoNeto1(argumento2);
			atributoPrueba.setAtributoNeto2(argumento3);
			atributoPrueba.setAtributoNeto3(argumento4);
			atributoPrueba.setAtributoNeto4(argumento5);
			atributoPrueba.setAtributoNeto5(argumento6);
			atributoPrueba.setAtributoNeto6(argumento7);
			atributoPrueba.setAtributoNeto7(argumento8);
			atributoPrueba.setAtributoNeto8(argumento9);
			atributoPrueba.setAtributoNeto9(argumento10);
			atributoPrueba.setAtributoNeto10(argumento11);
			atributoPrueba.setAtributoNeto11(argumento12);
			atributoPrueba.setAtributoNeto12(argumento13);
			atributoPrueba.setAtributoNeto13(argumento14);
			atributoPrueba.setAtributoNeto14(argumento15);
			atributoPrueba.setAtributoNeto15(argumento16);
			atributoPrueba.setAtributoNeto16(argumento17);
			atributoPrueba.setAtributoNeto17(argumento18);
			atributoPrueba.setAtributoNeto18(argumento19);
			atributoPrueba.setAtributoNeto19(argumento20);

			Run.lista.add(atributoPrueba);

			LOGGER.info("Se agrego objeto correctamente a la lista");
		} catch (Exception ex) {
			LOGGER.log(Level.SEVERE, "ERROR", ex);
		}

	}

	/**
	 * Metodo que te permite buscar un objeto de la lista que se tiene
	 * 
	 * @param argumento1 objeto a buscar dentro del arreglo
	 * @return Se obtiene el objeto que coincide con argumento1
	 */
	public Atributos buscarNombre(int argumento1) {
		Run.lista.forEach(new Consumer<Atributos>() {

			@Override
			public void accept(Atributos correr) {
				// TODO Auto-generated method stub
				if (correr.getAtributoNeto() == argumento1) {
					atributoPrueba = correr;
				}
			}
		});
		return atributoPrueba;
	}

	/**
	 * Metodo que imprime el argumento1 y argumento2 de la lista de atributos
	 * 
	 * 
	 */
	public void buscarTodos() {
		Run.lista.forEach(new Consumer<Atributos>() {

			@Override
			public void accept(Atributos correr) {
				// TODO Auto-generated method stub
				System.out.println("Atributo" + correr.getAtributoNeto() + "\nAtributo2: " + correr.getAtributoNeto1());
			}
		});
	}
}
