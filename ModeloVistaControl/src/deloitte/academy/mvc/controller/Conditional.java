package deloitte.academy.mvc.controller;

/**
 * 
 * @author jbaldovinos@externosdeloittemx.com
 * @version 1.0
 * @since 11/03/2020 La clase Conditionals tiene 5 metodos condicionales. Entre
 *        ellos numericos, booleanos, ternarios
 * 
 */

public class Conditional {

	/**
	 * Metodo que compara dos numeros enteros
	 * 
	 * @param x int numerico ingresado en la clase main
	 * @param y int numerico ingresado en la clase main
	 * @return s Te regresa un valor entero multiplicado por 100 dependiendo de la
	 *         condicion que se cumpliera
	 * 
	 */
	public static int Condicion1(int x, int y) {
		int s = (x > y) ? x / 100 : y / 100;

		return s;
	}

	/**
	 * Metodo que compara el valor ingresado con un valor predeterminado
	 * 
	 * @param x Valor numerico ingresado en la clase main
	 * @return x
	 */

	public static int Condicion2(int x) {
		String mensaje = x >= 50 ? "Hola mundo!" : "zzZZzzZZ..";

		return x;

	}

	/**
	 * Metodo que compara un estado booleano y dependiendo la condicion que se
	 * cumpla te regresa valor 100 o 0
	 * 
	 * @param x booleano ingresado por el usuario en la clase main
	 * @return x valor booleano
	 */

	public static boolean Condicion3(boolean x) {
		int mensaje = x == true ? 100 : 0;
		return x;

	}

	/**
	 * Metodo que compara dos numeros y en caso de que los numeros ingresados sean
	 * iguales te regresa el valor de x
	 * 
	 * @param x valor de tipo entero ingresado por el usuario en la clase main
	 * @param y valor de tipo entero ingresado por el usuario en la clase main
	 * @return x valor de tipo entero
	 */
	public static int Condicion4(int x, int y) {
		int resultado = 0;
		if (x == y) {
			resultado = x;
		}
		return resultado;

	}

	/**
	 * Metodo que te compara dos numeros enteros y en clase de que se cumpla una
	 * condicion te iguala el resultado a la condicion que se cumplio
	 * 
	 * @param x valor de tipo entero ingresado por el usuario en la clase main
	 * @param y valor de tipo entero ingresado por el usuario en la clase main
	 * @return resultado valor de tipo entero, el valor de resultado depende de la
	 *         condicion que se cumpla con los numeros ingresados con el usuario en
	 *         la clase tipo main
	 */
	public static int Condicion5(int x, int y) {
		int resultado;
		if (x > y) {
			resultado = x;
		} else {
			resultado = y;
		}

		return resultado;
	}
}
