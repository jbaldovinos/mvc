package deloitte.academy.mvc.model;

/**
 * Clase de tipo identidad Contiene 20 atributos
 * 
 * @author jbaldovinos@externosdeloittemx.com
 * @version 1.0
 * @since 11/03/2020
 *
 */
public class Atributos {
	/**
	 * Declaracion de variables
	 * 
	 */

	private int atributoNeto;
	private int atributoNeto1;
	private int atributoNeto2;
	private int atributoNeto3;
	private int atributoNeto4;
	private int atributoNeto5;
	private int atributoNeto6;
	private int atributoNeto7;
	private int atributoNeto8;
	private int atributoNeto9;
	private int atributoNeto10;
	private int atributoNeto11;
	private int atributoNeto12;
	private int atributoNeto13;
	private int atributoNeto14;
	private int atributoNeto15;
	private int atributoNeto16;
	private int atributoNeto17;
	private int atributoNeto18;
	private int atributoNeto19;

	public Atributos(String string, String string2, String string3, String string4, String string5, String string6,
			String string7, String string8, String string9, String string10, String string11, String string12,
			String string13, String string14, int i, int j, int k, int l, int m, int n) {
		// TODO Auto-generated constructor stub
	}

	public Atributos() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Getters y Setters
	 * 
	 */
	public int getAtributoNeto() {
		return atributoNeto;
	}

	public void setAtributoNeto(int atributoNeto) {
		this.atributoNeto = atributoNeto;
	}

	public int getAtributoNeto1() {
		return atributoNeto1;
	}

	public void setAtributoNeto1(int atributoNeto1) {
		this.atributoNeto1 = atributoNeto1;
	}

	public int getAtributoNeto2() {
		return atributoNeto2;
	}

	public void setAtributoNeto2(int atributoNeto2) {
		this.atributoNeto2 = atributoNeto2;
	}

	public int getAtributoNeto3() {
		return atributoNeto3;
	}

	public void setAtributoNeto3(int atributoNeto3) {
		this.atributoNeto3 = atributoNeto3;
	}

	public int getAtributoNeto4() {
		return atributoNeto4;
	}

	public void setAtributoNeto4(int atributoNeto4) {
		this.atributoNeto4 = atributoNeto4;
	}

	public int getAtributoNeto5() {
		return atributoNeto5;
	}

	public void setAtributoNeto5(int atributoNeto5) {
		this.atributoNeto5 = atributoNeto5;
	}

	public int getAtributoNeto6() {
		return atributoNeto6;
	}

	public void setAtributoNeto6(int atributoNeto6) {
		this.atributoNeto6 = atributoNeto6;
	}

	public int getAtributoNeto7() {
		return atributoNeto7;
	}

	public void setAtributoNeto7(int atributoNeto7) {
		this.atributoNeto7 = atributoNeto7;
	}

	public int getAtributoNeto8() {
		return atributoNeto8;
	}

	public void setAtributoNeto8(int atributoNeto8) {
		this.atributoNeto8 = atributoNeto8;
	}

	public int getAtributoNeto9() {
		return atributoNeto9;
	}

	public void setAtributoNeto9(int atributoNeto9) {
		this.atributoNeto9 = atributoNeto9;
	}

	public int getAtributoNeto10() {
		return atributoNeto10;
	}

	public void setAtributoNeto10(int atributoNeto10) {
		this.atributoNeto10 = atributoNeto10;
	}

	public int getAtributoNeto11() {
		return atributoNeto11;
	}

	public void setAtributoNeto11(int atributoNeto11) {
		this.atributoNeto11 = atributoNeto11;
	}

	public int getAtributoNeto12() {
		return atributoNeto12;
	}

	public void setAtributoNeto12(int atributoNeto12) {
		this.atributoNeto12 = atributoNeto12;
	}

	public int getAtributoNeto13() {
		return atributoNeto13;
	}

	public void setAtributoNeto13(int atributoNeto13) {
		this.atributoNeto13 = atributoNeto13;
	}

	public int getAtributoNeto14() {
		return atributoNeto14;
	}

	public void setAtributoNeto14(int atributoNeto14) {
		this.atributoNeto14 = atributoNeto14;
	}

	public int getAtributoNeto15() {
		return atributoNeto15;
	}

	public void setAtributoNeto15(int atributoNeto15) {
		this.atributoNeto15 = atributoNeto15;
	}

	public int getAtributoNeto16() {
		return atributoNeto16;
	}

	public void setAtributoNeto16(int atributoNeto16) {
		this.atributoNeto16 = atributoNeto16;
	}

	public int getAtributoNeto17() {
		return atributoNeto17;
	}

	public void setAtributoNeto17(int atributoNeto17) {
		this.atributoNeto17 = atributoNeto17;
	}

	public int getAtributoNeto18() {
		return atributoNeto18;
	}

	public void setAtributoNeto18(int atributoNeto18) {
		this.atributoNeto18 = atributoNeto18;
	}

	public int getAtributoNeto19() {
		return atributoNeto19;
	}

	public void setAtributoNeto19(int atributoNeto19) {
		this.atributoNeto19 = atributoNeto19;
	}
}